########################################
### Creds: #############################
########################################

provider "aws" {
    # access_key  = ""
    # secret_key  = ""

    # credentials are handled by system variables for obvious reasons

    region     = "${lookup(var.global,"region")}"
}

